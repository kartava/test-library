Books_Library
========================

Below you will find basic setup and deployment instructions for the books_library
project. To begin you should have the following applications installed on your
local development system:

- Python >= 2.7
- `pip <http://www.pip-installer.org/>`_ >= 1.5
- `virtualenv <http://www.virtualenv.org/>`_ >= 1.10
- `virtualenvwrapper <http://pypi.python.org/pypi/virtualenvwrapper>`_ >= 3.0
- Postgres >= 9.3
- git >= 1.7

Getting Started
------------------------

First clone the repository from Github and switch to the new directory::

    $ git clone git@github.com:[ORGANIZATION]/books_library.git
    $ cd books_library

To setup your local environment you can use the quickstart make target `setup`, which will
install both Python and Javascript dependencies (via pip and npm) into a virtualenv named
"books_library", configure a local django settings file, and create a database via
Postgres named "books_library" with all migrations run::

    $ make setup
    $ workon books_library

If you require a non-standard setup, you can walk through the manual setup steps below making
adjustments as necessary to your needs.

To setup your local environment you should create a virtualenv and install the
necessary requirements::

    # Check that you have python2.7 installed
    $ which python2.7
    $ mkvirtualenv books_library -p `which python2.7`
    (books_library)$ pip install -r requirements/dev.txt
    (books_library)$ npm install

Next, we'll set up our local environment variables. We use `django-dotenv
<https://github.com/jpadilla/django-dotenv>`_ to help with this. It reads environment variables
located in a file name ``.env`` in the top level directory of the project. The only variable we need
to start is ``DJANGO_SETTINGS_MODULE``::

    (books_library)$ cp books_library/settings/local.example.py books_library/settings/local.py
    (books_library)$ echo "DJANGO_SETTINGS_MODULE=books_library.settings.local" > .env

Create the Postgres database and run the initial migrate::

    (books_library)$ createdb -E UTF-8 books_library
    (books_library)$ python manage.py migrate

Add the initial data::

    (books_library)$ python manage.py loaddata authors.json
    (books_library)$ python manage.py loaddata books.json

You should now be able to run the development server::

    (books_library)$ python manage.py runserver