from django import forms

from books_library.books.models import Book
from books_library.common.forms import BootstrapMixin


class BookCreationForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Book
        fields = ('authors', 'title', 'description')
