from django.conf.urls import url

from books_library.books.views import BookCreateView, BookListView, BookDetailView, \
    BookUpdateView, BookDeleteView, search

urlpatterns = [
    url('^$', BookListView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/$', BookDetailView.as_view(), name='detail'),
    url('^create/$', BookCreateView.as_view(), name='create'),
    url('^(?P<pk>\d+)/delete/$', BookDeleteView.as_view(), name='delete'),
    url('^(?P<pk>\d+)/edit/$', BookUpdateView.as_view(), name='edit'),
    url(r'^search/', search, name='search'),
]
