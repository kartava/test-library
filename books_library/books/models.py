from __future__ import unicode_literals

from django.db import models


class Book(models.Model):
    authors = models.ManyToManyField('authors.Author')
    title = models.CharField(max_length=100)
    description = models.TextField()

    def get_authors(self):
        return ', '.join(str(author) for author in self.authors.all())

    def __unicode__(self):
        return self.title
