from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.http.response import JsonResponse
from django.utils.decorators import method_decorator
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView

from books_library.books.forms import BookCreationForm
from books_library.books.models import Book
from books_library.common.decorators import ajax_required
from books_library.common.views import ActiveMenuItemMixin


class BookListView(ActiveMenuItemMixin, ListView):
    model = Book
    context_object_name = 'books'
    template_name = 'books/list.html'
    active_menu_item = 'books'


class BookDetailView(DetailView):
    model = Book
    template_name = 'books/detail.html'
    context_object_name = 'book'


class BookCreateView(CreateView):
    model = Book
    form_class = BookCreationForm
    template_name = 'books/create.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(BookCreateView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self, **kwargs):
        return reverse('books:detail', kwargs={'pk': self.object.id})


class BookUpdateView(UpdateView):
    model = Book
    form_class = BookCreationForm
    template_name = 'books/create.html'

    def get_success_url(self, **kwargs):
        return reverse('books:detail', kwargs={'pk': self.object.id})

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(BookUpdateView, self).dispatch(request, *args, **kwargs)


class BookDeleteView(DeleteView):
    model = Book
    template_name = "books/confirm_delete.html"
    success_url = reverse_lazy('books:list')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(BookDeleteView, self).dispatch(request, *args, **kwargs)


@login_required
@ajax_required
def search(request):
    books = []
    q = request.GET.get('q')
    if q:
        books = Book.objects.filter(title__icontains=q)
    res = [{'id': book.id, 'title': book.title} for book in books]
    return JsonResponse({'result': res})
