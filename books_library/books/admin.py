from django.contrib import admin

from books_library.books.models import Book


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'get_authors')
    search_fields = ('title',)
