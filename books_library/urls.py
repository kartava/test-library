"""books_library URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import login, logout
from django.contrib.auth.decorators import user_passes_test
from django.views.generic import TemplateView

from books_library.common.views import RegisterFormView, LoginFormView

login_forbidden = user_passes_test(lambda u: u.is_anonymous(), '/books/', '')
urlpatterns = [
    # url(r'^$', TemplateView.as_view(template_name='base.html')),
    url(r'^admin/', admin.site.urls),
    url(r'^register/$', login_forbidden(RegisterFormView.as_view()), name="signup"),
    url(r'^login/$', login_forbidden(LoginFormView.as_view()),  name="login"),
    url(r'^logout/$', logout, name='logout', kwargs={'next_page': '/books/'}),
    url(r'^books/', include('books_library.books.urls', namespace='books')),
    url(r'^authors/', include('books_library.authors.urls', namespace='authors')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

