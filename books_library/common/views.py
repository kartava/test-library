from django.contrib.auth import login, authenticate
from django.views.generic.edit import FormView

from books_library.common.forms import CustomUserCreationForm, \
    CustomUserAuthenticationForm


class ActiveMenuItemMixin(object):
    def get_context_data(self, **kwargs):
        ctx = super(ActiveMenuItemMixin, self).get_context_data(**kwargs)
        ctx['active_menu_item'] = getattr(self, 'active_menu_item', None)
        return ctx


class RegisterFormView(ActiveMenuItemMixin, FormView):
    form_class = CustomUserCreationForm
    success_url = '/books/'
    template_name = 'registration/register.html'
    active_menu_item = 'signup'

    def form_valid(self, form):
        form.save()
        user = authenticate(username=form.data['username'],
                            password=form.data['password1'])
        login(self.request, user)
        return super(RegisterFormView, self).form_valid(form)


class LoginFormView(ActiveMenuItemMixin, FormView):
    form_class = CustomUserAuthenticationForm
    success_url = '/books/'
    template_name = 'registration/login.html'
    active_menu_item = 'login'

    def form_valid(self, form):
        user = authenticate(username=form.data['username'],
                            password=form.data['password'])
        login(self.request, user)
        return super(LoginFormView, self).form_valid(form)
