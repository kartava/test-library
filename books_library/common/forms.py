from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User


class BootstrapMixin(object):
    def __init__(self, *args, **kwargs):
        super(BootstrapMixin, self).__init__(*args, **kwargs)
        meta = getattr(self, 'Meta', object)
        bootstrap_exclude = getattr(meta, 'bootstrap_exclude', [])
        for field in self.fields:
            if field not in bootstrap_exclude:
                if self.fields[field].widget.__class__ not in [forms.CheckboxInput, forms.RadioSelect, forms.FileInput, forms.ClearableFileInput]:
                    cls = self.fields[field].widget.attrs.get('class', '')
                    cls += ' form-control'
                    self.fields[field].widget.attrs['class'] = cls
            self.fields[field].widget.attrs['placeholder'] = self.fields[field].label or field.capitalize()


class CustomUserCreationForm(BootstrapMixin, UserCreationForm):
    msg = 'Letters, digits and @/./+/-/_ only.'
    username = forms.CharField(widget=forms.TextInput(
        attrs={'rows': 5, 'placeholder': msg}))
    email = forms.EmailField(required=True, widget=forms.TextInput(
        attrs={'placeholder': 'example@mail.com'}))

    class Meta:
        model = User
        fields = ('username', 'email')


class CustomUserAuthenticationForm(BootstrapMixin, AuthenticationForm):

    class Meta:
        model = User
