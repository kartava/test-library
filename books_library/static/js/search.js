$(document).ready(function () {
  // Instantiate the Bloodhound suggestion engine
  var searchUrl = '/books/search/';
  var books = new Bloodhound({
    datumTokenizer: function (datum) {
      return Bloodhound.tokenizers.whitespace(datum.value);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      wildcard: '%QUERY',
      url: document.location.origin + searchUrl + '?q=%QUERY',
      ajax: {
        jsonp: 'callback',
        dataType: 'jsonp'
      },
      transform: function (response) {
        // Map the remote source JSON array to a JavaScript object array
        return $.map(response.result, function (book) {
          return {
            url: book.id,
            value: book.title
          };
        });
      }
    }
  });
  window.initTypeahead = function () {
    $('.typeahead')
      .typeahead(null, {
        display: 'value',
        source: books,
        templates: {
          empty: [
            '<p class="empty-message alert-info">',
            'Unable to find any book that match current query',
            '</p>'
          ].join('\n'),
          suggestion: Handlebars.compile('<a href="{{url}}" class="drop-down-item">{{value}}</a>')
        }
      })
      .on("keypress", function (event) {
        if (event.which == 13) {
          var name = 'q=' + $(this).val();
          getBooks(name);
        }
      });
  };
  function getBooks(data) {
    console.log(data);
    $.ajax({
      type: "GET",
      url: document.location.origin + searchUrl,
      dataType: 'json',
      data: data,
      success: function (data) {
        if (data.result.length !== 0) {
          var id = data.result[0].id;
          var url = '/books/' + id;
          window.location.replace(url)
        }
      }
    });
  }
});