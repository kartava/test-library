from django.conf.urls import url

from books_library.authors.views import AuthorCreateView, AuthorListView, \
    AuthorDetailView, AuthorUpdateView, AuthorDeleteView

urlpatterns = [
    url('^$', AuthorListView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/$', AuthorDetailView.as_view(), name='detail'),
    url('^create/$', AuthorCreateView.as_view(), name='create'),
    url('^(?P<pk>\d+)/delete/$', AuthorDeleteView.as_view(), name='delete'),
    url('^(?P<pk>\d+)/edit/$', AuthorUpdateView.as_view(), name='edit'),
]
