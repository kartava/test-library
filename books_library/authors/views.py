from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView, CreateView, UpdateView
from django.views.generic.list import ListView

from books_library.authors.forms import AuthorCreationForm
from books_library.authors.models import Author
from books_library.common.views import ActiveMenuItemMixin


class AuthorListView(ActiveMenuItemMixin, ListView):
    model = Author
    context_object_name = 'authors'
    template_name = 'authors/list.html'
    active_menu_item = 'authors'


class AuthorDetailView(DetailView):
    model = Author
    template_name = 'authors/detail.html'
    context_object_name = 'author'


class AuthorCreateView(CreateView):
    model = Author
    form_class = AuthorCreationForm
    template_name = 'authors/create.html'
    active_menu_item = 'create_author'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AuthorCreateView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('authors:detail', kwargs={'pk': self.object.id})


class AuthorUpdateView(UpdateView):
    model = Author
    form_class = AuthorCreationForm
    template_name = 'authors/create.html'

    def get_success_url(self, **kwargs):
        return reverse('authors:detail', kwargs={'pk': self.object.id})

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AuthorUpdateView, self).dispatch(request, *args, **kwargs)


class AuthorDeleteView(DeleteView):
    model = Author
    template_name = "authors/confirm_delete.html"
    success_url = reverse_lazy('authors:list')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AuthorDeleteView, self).dispatch(request, *args, **kwargs)

