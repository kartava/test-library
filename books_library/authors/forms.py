from django import forms

from books_library.authors.models import Author
from books_library.common.forms import BootstrapMixin


class AuthorCreationForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Author
        fields = ('first_name', 'last_name')
