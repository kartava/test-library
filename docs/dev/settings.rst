Project settings
================

This only lists settings that have docstrings. See the relevant
settings files for the complete settings, or run::

    $ python manage.py diffsettings

Base settings
-------------

.. automodule:: books_library.settings.base
   :members:

Deploy settings
---------------

.. automodule:: books_library.settings.deploy
   :members:

Development settings
--------------------

.. automodule:: books_library.settings.dev
   :members:
